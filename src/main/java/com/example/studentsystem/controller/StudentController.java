package com.example.studentsystem.controller;


import com.example.studentsystem.domain.Student;
import com.example.studentsystem.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class StudentController {
    @Autowired
    StudentService studentService;

    @RequestMapping("/student1/list1")
    public String list(Model model){
        List<Student> students=studentService.findAll();
        model.addAttribute("data",students);

        return "/student1/list1";
    }
}


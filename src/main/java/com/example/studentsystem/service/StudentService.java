package com.example.studentsystem.service;


import com.example.studentsystem.domain.Student;
import com.example.studentsystem.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public List<Student> findAll(){
        return studentRepository.findAll();
    }
    public Student findById(Integer id){

        return studentRepository.findById(id).orElse(null);
    }
    /**
     * 对name进行查询
     * @param name
     * @return
     */
    public List<Student> findByName(String name){

        return studentRepository.findByNameLike(name);
    }
    /**
     * 对tb_student进行插入数据
     * @param student1
     * @return
     */
    public Student insert(Student student1){
        studentRepository.save(student1);
        return student1;
    }
    /**
     * 对tb_student进行更新数据
     * @param student1
     * @return
     */
    public Student update(Student student1){
        studentRepository.save(student1);
        return student1;
    }
    /**
     * 对tb_student进行删除数据
     * @param id
     * @return
     */
//    public void delete(Student student){
//        studentRepository.delete(student);
//    }
    public void delete(Integer id){
        Student student1=new Student();
        student1.setId(id);
        studentRepository.delete(student1);
    }
    public Student getById(Integer id){
        Student student1=studentRepository.findById(id).orElse(null);
        return student1;
    }
}

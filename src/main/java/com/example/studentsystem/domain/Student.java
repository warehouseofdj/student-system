package com.example.studentsystem.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 针对 tb_student的实体类
 */
@Data
@Entity(name="tb_student1")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 学号
     */
    private Integer no;
    /**
     * 姓名
     */
    private String name;
    /**
     * 密码
     */
    private String password;
    /**
     * 性别，0未知，1男，2女
     */
    private Integer sex;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 成绩
     */
    private Integer score;
}
